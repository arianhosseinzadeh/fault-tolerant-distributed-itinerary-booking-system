package TM;

import java.rmi.ConnectException;
import java.rmi.RemoteException;
import java.rmi.UnmarshalException;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.Vector;


import ResImpl.ResourceManagerImpl;
import ResInterface.InvalidTransactionException;
import ResInterface.ResourceManager;
import ResInterface.TransactionAbortedException;


public class TransactionManager {
	Hashtable<Integer, Vector<Integer>> TList;
	Hashtable<Integer, Boolean> timedOuts;
	Vector<TransactionTime> TransactionTimeList;
	ResourceManagerImpl rm;
	TimeChecker backgroundTimer;
	int timeout = 50;


	public TransactionManager(ResourceManagerImpl m) {
		TList = new Hashtable<Integer, Vector<Integer>>();
		timedOuts = new Hashtable<Integer, Boolean>();
		TransactionTimeList = new Vector<TransactionTime>();
		backgroundTimer = new TimeChecker(TransactionTimeList, timedOuts, this);
		rm = m;
		backgroundTimer.start();
	}


	public void prepare(int TID) throws InvalidTransactionException{
		Vector<Integer> c = TList.get(TID);
		if(c == null){
			throw new InvalidTransactionException("No such Transaction");
		}
		for(int i = 0 ; i < TransactionTimeList.size();i++){
			if(TransactionTimeList.get(i).getTxid() == TID){
				TransactionTimeList.remove(i);
				break;
			}
		}
	}

	public int start(int TID) throws RemoteException{
		Vector<Integer> transactionRMList = new Vector<Integer>();
		synchronized(TList){
			TList.put(TID, transactionRMList);
		}
		TransactionTime t = new TransactionTime(TID, timeout);
		TransactionTimeList.add(t);
		return TID;
	}

	public void enlist(int txid, int  rm) throws InvalidTransactionException, TransactionAbortedException{
		Vector<Integer> c = TList.get(txid);
		if(c == null){
			throw new InvalidTransactionException("No such Transaction");
		}
		if(timedOuts.get(txid) != null){
			timedOuts.remove(txid);
			throw new TransactionAbortedException("Transaction already timed out");
		}
		for(int i = 0 ; i < TransactionTimeList.size();i++){
			if(TransactionTimeList.get(i).getTxid() == txid){
				TransactionTimeList.get(i).setTxtime(timeout);
				break;
			}
		}
		boolean alreadyenlisted = false;
		for(int i = 0 ; i < c.size();i++){
			if(c.get(i) == rm)
				alreadyenlisted = true;
		}
		if(!alreadyenlisted){
			c.add(rm);
		}
	}

	public boolean commit(int transactionId) throws RemoteException, TransactionAbortedException,
	InvalidTransactionException{
		Vector<Integer> c = TList.get(transactionId);
		if(c == null){
			throw new InvalidTransactionException("No such Transaction");
		}
		if(timedOuts.get(transactionId) != null){
			timedOuts.remove(transactionId);
			throw new TransactionAbortedException("Transaction already timed out");
		}
		for(int i = 0 ; i < TransactionTimeList.size();i++){
			if(TransactionTimeList.get(i).getTxid() == transactionId){
				TransactionTimeList.remove(i);
				break;
			}
		}
		TList.remove(transactionId);
		return true;
	}

	public void abort(int transactionId) throws RemoteException, InvalidTransactionException{
		Vector<Integer> c = TList.get(transactionId);
		if(c == null){
			throw new InvalidTransactionException("No such Transaction");

		}
		for(int i = 0 ; i < TransactionTimeList.size();i++){
			if(TransactionTimeList.get(i).getTxid() == transactionId){
				TransactionTimeList.remove(i);
				break;
			}
		}
		TList.remove(transactionId);
	}

	public void tmabort(int transactionId) throws RemoteException, InvalidTransactionException{
		Vector<Integer> c = TList.get(transactionId);
		if(c == null){
			throw new InvalidTransactionException("No such Transaction");
		}
		for(int i = 0 ; i < TransactionTimeList.size();i++){
			if(TransactionTimeList.get(i).getTxid() == transactionId){
				TransactionTimeList.remove(i);
				break;
			}
		}
		TList.remove(transactionId);
		rm.abort(transactionId,false);
	}
}
