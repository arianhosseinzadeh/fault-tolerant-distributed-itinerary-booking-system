package ResInterface;

import java.rmi.RemoteException;

public class InvalidTransactionException extends RemoteException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidTransactionException() {
		// TODO Auto-generated constructor stub
	}

	public InvalidTransactionException(String s) {
		super(s);
		// TODO Auto-generated constructor stub
	}

	public InvalidTransactionException(String s, Throwable cause) {
		super(s, cause);
		// TODO Auto-generated constructor stub
	}

}
