# Distributing an Itinerary Service (Middleware Approach)

## Description ##

In three phases of this project, we extended and developed an itinerary reservation system.

In the first phase we distributed the system from a client-server model to the client-middleware-server model, a 3-tier architecture, by using two different approaches to connect the layers: TCP/IP and RMI.We also separated methods and data structures belonging to each of the servers : Cars, Flights, and Hotels into three different java classes. We connected each tier to its lower one using two ways of connection mentioned earlier.

![rsz_screen_shot_2015-09-24_at_53057_pm.png](https://bitbucket.org/repo/559A84/images/3207200328-rsz_screen_shot_2015-09-24_at_53057_pm.png)

In the second phase, we distributed the information of customers on the servers instead of keeping it in a single point (i.e. the middle ware). We also kept the reservation information for each data item/customer on the server which is in charge of that data item , in the other words when we create a customer we replicate the customer information on all of the servers, when we want to reserve an item for that customer we do the reservation only on the corresponding server, in case we want to delete this customer we delete the customer information on all of the servers and finally when we want to query the customer information we request information of that specific customer from all of the servers.

A lock manager was added to the system that provides the "ISOLATION". We decided to keep this Lock Manager in the middleware so there would be no need of detecting distributed deadlocks on the servers. Also, a Transaction Manager was added to the middleware which keeps track of transactions and resource managers (i.e servers). To prevent indefinite deadlocks, we defined a time-out, which means each transaction that is waiting for a lock or not doing anything (commanded by the client) would time-out after this amount of time, so other transactions may use the resources which are probably held by these inactive transactions.

We designed the lock manager as if a transaction requests a lock on an object, it may acquire the read lock if it already has a write lock on the same object and it gains a write lock if it already has a read lock on that item and there’s no other transaction having a read lock request waiting, in other cases it would wait until the lock is assigned to it.

"ATOMICITY" was also implemented in this phase by use of Transaction Manager mentioned earlier. A set of operations in a transaction would change the data (stored in a hashtable) on each server after committing the transaction while aborting the transaction causes no effect on the data. The system takes a backup of the original data, writes the changes to the datastore of the server and replaces the data by the backup version in case of abort.

In the third phase of this project, we implemented 2-phase commit (2PC) functionality. In order to achieve 2PC, we used voting model, which means when a client wants to commit a transaction:

1. the middleware sends VOTE REQUEST to the servers

2. Server writes its vote in a log file and then sends it to the middleware

3. If middleware receives all votes as YES : middleware sends COMMIT to all of the server and they will commit, otherwise it will send ABORT to those servers which had sent YES vote.

We also extended the project to provide DURABILITY which means that it is able to recover from crashes.
First we wanted to be able to simulate crashes. We implemented a method in the middleware that as soon as it was called by the mock client an arbitrary server would crash. The supported types of crashes comes in the following and they may happen in two different layers: middleware layer and server layer:

For transaction manager in middleware:
* Crash before sending vote request

* Crash after sending vote request and before receiving any replies

* Crash after receiving some replies but not all

* Crash after receiving all replies but before deciding

* Crash after deciding but before sending decision

* Crash after sending some but not all decisions

* Crash after having sent all decisions

For the resource manager:

* Crash after receive vote request but before sending answer

* Crash after sending answer

* Crash after receiving decision but before committing/aborting

Each resource manager creates three types of files: log file, recent file, data file. 
Log file stores the last status of a transaction. 
Recent file stores the most recent changes (add, delete and modify) on the storage data items such as flight, car, room or customer information. 
Data file keeps the information of all of the items. 
During the process of a transaction, all files are stored as temporary files. If no crash happens, the temporary files will be renamed to master files. To restore the lost information regarding the status of the last transaction, these files will be used. 

To simulate a crash at the middleware, long sleep time is used; In case that the middleware crashes a transaction on resource manager would be aborted. 
There are different scenarios of recovery from different crashes, but in all of them each server must be able to connect back to the middleware when it's up again.

![rsz_screen_shot_2015-09-24_at_55643_pm.png](https://bitbucket.org/repo/559A84/images/3601616410-rsz_screen_shot_2015-09-24_at_55643_pm.png)

## Testing

We tested the features incrementally after they were added in each phase. The tests were done both manually and automatically. 

### Phase 1 ###
One client was used to commanded the middleware to create/delete customers, to create/delete items (flight, car, room) and also to do the itinerary reservation. We checked the results and it passed.

### Phase 2:
Two clients were used to send commands to the middleware at the same time. Deadlock and Time-out situations were considered and tested by use of conflicting commands. Durability and effect of a commit and an abort was also tested. We also requested a lock to see if a transaction holds an exclusive lock, what another transaction would do when it requests a lock on the same object. 

### Phase 3:
Two clients were used to send commands to the middleware concurrently. Crash cases were simulated (i.e. by forcing the resource manager/transaction manager to crash). Then the crashed node restarted to test the recovering process by use of the log files.