package TM;

import java.rmi.ConnectException;
import java.rmi.RemoteException;
import java.rmi.UnmarshalException;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.Vector;

import LockManager.LockManager;
import MiddlewareImpl.MiddlewareImpl;
import ResInterface.InvalidTransactionException;
import ResInterface.ResourceManager;
import ResInterface.TransactionAbortedException;

public class TransactionManager {
	Hashtable<Integer, Vector<Integer>> TList;
	Hashtable<Integer, Boolean> timedOuts;
	Vector<TransactionTime> TransactionTimeList;
	MiddlewareImpl mw;
	LockManager lm;
	TimeChecker backgroundTimer;
	int timeout = 90;
	int crashtime = 60000;

	public TransactionManager(MiddlewareImpl m, LockManager l) {
		TList = new Hashtable<Integer, Vector<Integer>>();
		timedOuts = new Hashtable<Integer, Boolean>();
		TransactionTimeList = new Vector<TransactionTime>();
		backgroundTimer = new TimeChecker(TransactionTimeList, timedOuts, this);
		lm=l;
		mw = m;
		backgroundTimer.start();
	}

	public int start() throws RemoteException{
		Vector<Integer> transactionRMList = new Vector<Integer>();
		int txid = Integer.parseInt(
				String.valueOf(Calendar.getInstance().get(Calendar.MILLISECOND)) +
				String.valueOf( Math.round( Math.random() * 100 + 1 )));
		synchronized(TList){
			TList.put(txid, transactionRMList);
		}
		TransactionTime t = new TransactionTime(txid, timeout);
		TransactionTimeList.add(t);
		return txid;
	}

	public void enlist(int TID, int  serverType) throws InvalidTransactionException, TransactionAbortedException{// serverType: 0:car 1:flight 2:hotel
		Vector<Integer> c = TList.get(TID);
		if(c == null){
			throw new InvalidTransactionException("No such Transaction");
		}
		if(timedOuts.get(TID) != null){
			timedOuts.remove(TID);
			throw new TransactionAbortedException("Transaction already timed out");
		}
		for(int i = 0 ; i < TransactionTimeList.size();i++){
			if(TransactionTimeList.get(i).getTxid() == TID){
				TransactionTimeList.get(i).setTxtime(timeout);
				break;
			}
		}
		boolean alreadyenlisted = false;
		for(int i = 0 ; i < c.size();i++){
			if(c.get(i) == serverType)
				alreadyenlisted = true;
		}
		if(!alreadyenlisted){
			try {
				mw.resourceManager[serverType].start(TID);
				c.add(serverType);
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
	}


	public boolean commit(int TID) throws RemoteException, TransactionAbortedException,
	InvalidTransactionException{
		Vector<Integer> c = TList.get(TID);
		if(c == null){
			lm.UnlockAll(TID);
			throw new InvalidTransactionException("No such Transaction");
		}
		if(timedOuts.get(TID) != null){
			timedOuts.remove(TID);
			lm.UnlockAll(TID);
			throw new TransactionAbortedException("Transaction already timed out");
		}
		//Before vote request 
		if(mw.crashLocation.toLowerCase().equals("bsvr")){//before sending vote request
			System.out.println("Crash before sending vote request");
			fakeCrash();
		}
		Vector<Boolean> voteResponces = new Vector<Boolean>();
		//After vote request
		boolean vote = true;
		for (int i = 0 ; i < c.size();i++){
			try{
				boolean t = mw.resourceManager[c.get(i)].prepare(TID);
				if(mw.crashLocation.toLowerCase().equals("brar")){//after sending vote request and before receiving any replies
					System.out.println("Crash after sending vote request and before receiving any replies");
					fakeCrash();
				}
				if(i==1)
					if(mw.crashLocation.toLowerCase().equals("arsr")){//after receiving some replies
						System.out.println("Crash after receiving some replies but not all");
						fakeCrash();
					}
				voteResponces.add(t);
			}
			catch (InvalidTransactionException e) {
				voteResponces.add(false);
				System.out.println("Transaction already timed out");
			}
			catch(TransactionAbortedException e){
				voteResponces.add(false);
				System.out.println("Transaction already timed out");
			}
			catch(UnmarshalException e){
				System.out.println("server crashed");
				voteResponces.add(false);
			}
			catch (RemoteException e) {
				voteResponces.add(false);
				System.out.println("Transaction already timed out");
			}	
		}
		//Before making decision
		if(mw.crashLocation.toLowerCase().equals("bd")){//before deciding
			System.out.println("Crash after receiving all replies but before deciding");
			fakeCrash();
		}
		for (int i = 0 ; i < c.size();i++){
			vote = vote & voteResponces.get(i); 
		}
		//After making decision - Before Sending Decision
		if(mw.crashLocation.toLowerCase().equals("bsd")){//before sending decision

			System.out.println("Crash after deciding but before sending decision");
			fakeCrash();
		}
		if(vote){
			System.out.println("Decision is COMMIT");
			for (int i = 0 ; i < c.size();i++){
				try{
					mw.resourceManager[c.get(i)].commit(TID,true);
					//After Sending some Decisions
					if(i==1)
						if(mw.crashLocation.toLowerCase().equals("assd")){//after sending some decisions
							System.out.println("Crash after deciding but before sending all decisions");
							fakeCrash();
						}
				}
				catch(UnmarshalException e){
					System.out.println("Resource Manager "+i+" crashed");
				}
				catch(ConnectException e){
					System.out.println("Resource Manager "+i+" crashed");
				}
				catch (InvalidTransactionException e) {
					System.out.println("Transaction already timed out");
				}
				catch(TransactionAbortedException e){
					System.out.println("Transaction already timed out");
				}
				catch (RemoteException e) {
					System.out.println("Transaction already timed out");
				}	
			}
		}
		else {
			mw.abort(TID);
			return false;
		}
		for(int i = 0 ; i < TransactionTimeList.size();i++){
			if(TransactionTimeList.get(i).getTxid() == TID){
				TransactionTimeList.remove(i);
				break;
			}
		}
		TList.remove(TID);
		lm.UnlockAll(TID);
		//After Sending all Decisions
		if(mw.crashLocation.toLowerCase().equals("asad")){//after sending all decisions
			System.out.println("Crash after having sent all decisions");
			fakeCrash();
		}
		return vote;
	}

	public void abort(int transactionId) throws RemoteException, InvalidTransactionException{
		Vector<Integer> c = TList.get(transactionId);
		if(c == null){
			lm.UnlockAll(transactionId);
			throw new InvalidTransactionException("No such Transaction");
		}
		for (int i = 0 ; i < c.size();i++){
			try{
				mw.resourceManager[c.get(i)].abort(transactionId,true);
			}
			catch (InvalidTransactionException e) {
				System.out.println("Transaction already timed out");
			}
			catch(TransactionAbortedException e){
				System.out.println("Transaction already timed out");
			}
			catch(ConnectException e){
				System.out.println("Resource Manager "+i+" crashed");
			}
			catch(UnmarshalException e){
				System.out.println("Resource Manager "+i+" crashed");
			}
			catch (RemoteException e) {
				System.out.println("Transaction already timed out");
			}
		}
		for(int i = 0 ; i < TransactionTimeList.size();i++){
			if(TransactionTimeList.get(i).getTxid() == transactionId){
				TransactionTimeList.remove(i);
				break;
			}
		}
		TList.remove(transactionId);
		lm.UnlockAll(transactionId);
	}

	public boolean shutdown() throws RemoteException{
		if(TList.size() != 0)
			return false;
		int shutDown = 0;
		for(int i = 0 ; i < mw.resourceManager.length ;i++){
			try{mw.resourceManager[i].shutdown();}
			catch (RemoteException e) {
				shutDown++;
				if (shutDown == 3) return true;
			}
		}
		return false;
	}

	private void fakeCrash(){
		try {
			System.out.println("Middleware crashed(sleep mode).");
			Thread.currentThread().sleep(crashtime);
			System.out.println("Middleware returned.");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		mw.crashLocation = "";
	}
}
