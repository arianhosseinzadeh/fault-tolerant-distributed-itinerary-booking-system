package TM;

import java.rmi.RemoteException;
import java.util.Hashtable;
import java.util.Vector;
import java.lang.Thread;

import LockManager.LockManager;
import ResInterface.InvalidTransactionException;

public class TimeChecker extends Thread {
	
	Vector<TransactionTime> list;
	Hashtable<Integer, Boolean> exceptions;
	TransactionManager tm;
	LockManager lm;
	
	
	
	public TimeChecker(Vector<TransactionTime> t, Hashtable<Integer, Boolean> e, TransactionManager m) {
	list = t;
	exceptions = e;
	tm = m;
	}

	@Override
	public void run() {
		while(true){
			
			for(int i = 0 ; i < list.size();i++){
				TransactionTime e = list.get(i);
				e.setTxtime(e.getTxtime()-1);
				
				if(e.getTxtime() == 0){
					
					try {
						tm.abort(e.getTxid());
						
					} catch (InvalidTransactionException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (RemoteException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					System.out.println("Transaction ID: " + e.getTxid() + " Timed out");
					exceptions.put(e.getTxid(), new Boolean(true));

					
				}
				
			}
			
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			
			
		}
		
	}

}
