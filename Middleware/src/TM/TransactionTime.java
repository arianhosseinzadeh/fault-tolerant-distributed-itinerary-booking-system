package TM;

public class TransactionTime {
	private int txid;
	private int txtime;
	public TransactionTime(int tx, int time) {
		setTxid(tx);
		setTxtime(time);
	}
	public void setTxid(int txid) {
		this.txid = txid;
	}
	public int getTxid() {
		return txid;
	}
	public void setTxtime(int txtime) {
		this.txtime = txtime;
	}
	public int getTxtime() {
		return txtime;
	}

}
