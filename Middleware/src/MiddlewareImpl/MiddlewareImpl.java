package MiddlewareImpl;



import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RMISecurityManager;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;


import java.util.*; 
import java.rmi.*;
import java.util.ArrayList.*;


import ResInterface.InvalidTransactionException;
import ResInterface.ResourceManager;
import ResInterface.TransactionAbortedException;
import TM.TransactionManager;
import LockManager.DeadlockException;
import LockManager.LockManager;




public class MiddlewareImpl extends java.rmi.server.UnicastRemoteObject implements ResourceManager
{
	protected Hashtable<Integer,String >  transactionStatus = null;
	static int resourceManagerTotalNumber = 3;
	static LockManager lockManager = new LockManager();
	TransactionManager transactionManager = new TransactionManager(this,lockManager);
	static String MiddlewareServer;
	Object lockobject = new Object();
	static String[] resourceManagers = null;
	static String message = "blank";
	static public ResourceManager [] resourceManager = null;
	public String crashLocation = "";

	public static void main(String args[])
	{

		resourceManager = new ResourceManager[resourceManagerTotalNumber];
		resourceManagers = new String[resourceManagerTotalNumber];

		for(int i = 0 ; i < resourceManagerTotalNumber; i++){
			resourceManagers[i] = args[i];
		}

		MiddlewareServer = "localhost";
		if (System.getSecurityManager() == null) {
			System.setSecurityManager(new RMISecurityManager());
		}
		try {
			MiddlewareImpl m = new MiddlewareImpl();
			Naming.rebind("Group1" + MiddlewareServer + "Middleware",m);
			System.out.println(MiddlewareServer+" M is running...");
		} 
		catch (Exception e) {
			System.err.println("Server exception: " + e.toString());
			e.printStackTrace();
		} 

	}

	protected MiddlewareImpl() throws RemoteException {transactionStatus = new Hashtable<Integer, String>();}

	public int getInt(Object temp) throws Exception {
		try {
			return (new Integer((String)temp)).intValue();
		}
		catch(Exception e) {
			throw e;
		}
	}

	public String getString(Object temp) throws Exception {
		try {	
			return temp.toString();
		}
		catch (Exception e) {
			throw e;
		}
	}

	@Override
	public  boolean addCars(int id, String location, int numCars, int price)
			throws RemoteException {
		try{
			lockManager.Lock(id, "car"+location, LockManager.WRITE);
			transactionManager.enlist(id, 0);
			if(resourceManager[0].addCars(id,location,numCars,price))
				System.out.println("Cars added");
			else
			{
				System.out.println("Cars could not be added");
				return false;
			}
		}

		catch(DeadlockException e){
			System.out.println("Deadlock Happened " + id);
			this.abort(id);
			throw new TransactionAbortedException("This Transaction Aborted "+id);

		}
		catch(RemoteException e){
			System.out.println("TransactionException Happened " + id);
			e.getMessage();
			this.abort(id);
			throw e;

		}
		catch(Exception e){
			System.out.println("EXCEPTION:");
			System.out.println(e.getMessage());
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public  boolean addFlight(int id, int flightNumber, int flightSeats, int flightPrice)
			throws RemoteException {
		long time = System.currentTimeMillis();
		try{
			lockManager.Lock(id,"flight"+flightNumber, LockManager.WRITE);
			transactionManager.enlist(id, 1);
			if(resourceManager[1].addFlight(id,flightNumber,flightSeats,flightPrice))
				System.out.println("Flight added");
			else
			{
				System.out.println("Flight could not be added");

				return false;
			}
		}
		catch(DeadlockException e){
			System.out.println("Deadlock Happened " + id);
			this.abort(id);
			throw new TransactionAbortedException("This Transaction Aborted "+id);

		}
		catch(RemoteException e){
			System.out.println("TransactionException Happened " + id);
			e.getMessage();
			this.abort(id);
			throw e;

			//				return false;
		}

		catch(Exception e){
			System.out.println("EXCEPTION:");
			System.out.println(e.getMessage());
			e.printStackTrace();

			return false;
		}



		return true;
	}

	@Override
	public  boolean addRooms(int id, String location, int numRooms, int price)
			throws RemoteException {
		try{
			lockManager.Lock(id, "room"+location, LockManager.WRITE);
			transactionManager.enlist(id, 2);
			if(resourceManager[2].addRooms(id,location,numRooms,price))
				System.out.println("Rooms added");
			else
			{
				System.out.println("Rooms could not be added");

				return false;
			}
		}
		catch(DeadlockException e){
			System.out.println("Deadlock Happened " + id);
			this.abort(id);
			throw new TransactionAbortedException("This Transaction Aborted "+id);

		}
		catch(RemoteException e){
			System.out.println("TransactionException Happened " + id);
			e.getMessage();
			this.abort(id);
			throw e;

		}
		catch(Exception e){
			System.out.println("EXCEPTION:");
			System.out.println(e.getMessage());
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public  boolean deleteCars(int id, String location) throws RemoteException {
		try{
			lockManager.Lock(id, "car"+location, LockManager.WRITE);
			transactionManager.enlist(id, 0);
			if(resourceManager[0].deleteCars(id,location))
				System.out.println("Cars Deleted");
			else
			{
				System.out.println("Cars could not be deleted");
				return false;
			}
		}
		catch(DeadlockException e){
			System.out.println("Deadlock Happened " + id);
			this.abort(id);
			throw new TransactionAbortedException("This Transaction Aborted "+id);

		}
		catch(RemoteException e){
			System.out.println("TransactionException Happened " + id);
			e.getMessage();
			this.abort(id);
			throw e;

		}
		catch(Exception e){
			System.out.println("EXCEPTION:");
			System.out.println(e.getMessage());
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public  boolean deleteCustomer(int id, int cid) throws RemoteException {
		try{
			boolean isPossible=true; 
			lockManager.Lock(id, getString(new Integer(cid)), LockManager.WRITE);
			for(int i=0;i<resourceManagerTotalNumber;i++)
			{
				transactionManager.enlist(id, i);
				if(!resourceManager[i].deleteCustomer(id,cid))
					isPossible = false;
			} 
			if(isPossible){
				System.out.println("Customer Deleted");
				return true;
			}
			else{
				System.out.println("Customer could not be deleted");
				return false;
			}

		}
		catch(DeadlockException e){
			System.out.println("Deadlock Happened " + id);
			this.abort(id);
			throw new TransactionAbortedException("This Transaction Aborted "+id);

		}
		catch(RemoteException e){
			System.out.println("TransactionException Happened " + id);
			e.getMessage();
			this.abort(id);
			throw e;

		}
		catch(Exception e){
			System.out.println("EXCEPTION:");
			System.out.println(e.getMessage());
			e.printStackTrace();
			return false;
		}

	}

	@Override
	public  boolean deleteFlight(int id, int flightNumber) throws RemoteException {
		try{
			lockManager.Lock(id, "flight"+flightNumber, LockManager.WRITE);
			transactionManager.enlist(id, 1);
			if(resourceManager[1].deleteFlight(id,flightNumber))
				System.out.println("Flight Deleted");
			else
			{
				System.out.println("Flight could not be deleted");
				return false;
			}
		}
		catch(DeadlockException e){
			System.out.println("Deadlock Happened " + id);
			this.abort(id);
			throw new TransactionAbortedException("This Transaction Aborted "+id);

		}
		catch(RemoteException e){
			System.out.println("TransactionException Happened " + id);
			e.getMessage();
			this.abort(id);
			throw e;

		}
		catch(Exception e){
			System.out.println("EXCEPTION:");
			System.out.println(e.getMessage());
			e.printStackTrace();
			return false;
		}
		return true;
	}


	@Override
	public boolean deleteRooms(int id, String location) throws RemoteException {
		try{
			lockManager.Lock(id, "room"+location, LockManager.WRITE);
			transactionManager.enlist(id, 2);
			if(resourceManager[2].deleteRooms(id,location))
				System.out.println("Rooms Deleted");
			else
			{
				System.out.println("Rooms could not be deleted");
				return false;
			}
		}
		catch(DeadlockException e){
			System.out.println("Deadlock Happened " + id);
			this.abort(id);
			throw new TransactionAbortedException("This Transaction Aborted "+id);

		}
		catch(RemoteException e){
			System.out.println("TransactionException Happened " + id);
			e.getMessage();
			this.abort(id);
			throw e;

		}
		catch(Exception e){
			System.out.println("EXCEPTION:");
			System.out.println(e.getMessage());
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public boolean itinerary(int id, int cid, Vector flightNumbers, String location,
			boolean Car, boolean Room) throws RemoteException {

		//        boolean can=true;
		int Id = id;

		//        boolean [] sucFlight = new boolean[arg2.size()];

		try{
			for(int i=0;i<flightNumbers.size();i++){
				int flightNumber = getInt(flightNumbers.get(i));
				transactionManager.enlist(Id, 1);
				lockManager.Lock(Id, "flight"+flightNumber, LockManager.READ);
				if(resourceManager[1].queryFlight(Id,flightNumber) < 1 ){
					return false;
				}
			}
		}
		catch(DeadlockException e){
			System.out.println("Deadlock Happened " + id);
			this.abort(id);
			throw new TransactionAbortedException("This Transaction Aborted "+id);

		}
		catch(RemoteException e){
			System.out.println("TransactionException Happened " + id);
			e.getMessage();
			this.abort(id);
			throw e;

		}
		catch(Exception e){
			System.out.println("EXCEPTION:");
			System.out.println(e.getMessage());
			e.printStackTrace();
			return false;
		}
		///
		try{
			if(Car){
				transactionManager.enlist(Id, 0);
				lockManager.Lock(Id, "car"+location, LockManager.READ);
				if(resourceManager[0].queryCars(Id, location)<1){return false;}
			}
		}
		catch(DeadlockException e){
			System.out.println("Deadlock Happened " + id);
			this.abort(id);
			throw new TransactionAbortedException("This Transaction Aborted "+id);

		}
		catch(RemoteException e){
			System.out.println("TransactionException Happened " + id);
			e.getMessage();
			this.abort(id);
			throw e;

		}
		catch(Exception e){
			System.out.println("EXCEPTION:");
			System.out.println(e.getMessage());
			e.printStackTrace();
			return false;

		}

		try{
			if(Room)
			{
				lockManager.Lock(Id, "room"+location, LockManager.READ);
				transactionManager.enlist(Id, 2);
				if(resourceManager[2].queryRooms(Id, location) < 1){return false;}
			}
		}
		catch(DeadlockException e){
			System.out.println("Deadlock Happened " + id);
			this.abort(id);
			throw new TransactionAbortedException("This Transaction Aborted "+id);

		}
		catch(RemoteException e){
			System.out.println("TransactionException Happened " + id);
			e.getMessage();
			this.abort(id);
			throw e;

		}
		catch(Exception e){

			System.out.println("EXCEPTION:");
			System.out.println(e.getMessage());
			e.printStackTrace();
			return false;

		}
		//getting all lockes all together
		try{
			synchronized(lockobject){
				for(int i=0;i<flightNumbers.size();i++){
					int flightNum = getInt(flightNumbers.get(i));
					lockManager.Lock(Id,"flight"+flightNum,LockManager.WRITE);
				}
				lockManager.Lock(id,"car"+location,LockManager.WRITE);
				lockManager.Lock(id,"room"+location,LockManager.WRITE);
				lockManager.Lock(id,getString(new Integer(cid)),LockManager.WRITE);
			}
		}
		catch(DeadlockException e){
			System.out.println("Deadlock Happened " + id);
			this.abort(id);
			throw new TransactionAbortedException("This Transaction Aborted "+id);

		}
		catch(RemoteException e){
			System.out.println("TransactionException Happened " + id);
			e.getMessage();
			this.abort(id);
			throw e;

		}
		catch(Exception e){
			System.out.println("EXCEPTION:");
			System.out.println(e.getMessage());
			e.printStackTrace();
			return false;
		}

		try{
			for(int i=0;i<flightNumbers.size();i++){
				transactionManager.enlist(Id, 1);
			}
		}

		catch(Exception e){
			System.out.println("EXCEPTION:");
			System.out.println(e.getMessage());
			e.printStackTrace();
			return false;
		}
		///

		try{
			if(Car){transactionManager.enlist(Id, 0);}
		}
		catch(Exception e){
			System.out.println("EXCEPTION:");
			System.out.println(e.getMessage());
			e.printStackTrace();
			return false;
		}
		///
		try{
			if(Room){transactionManager.enlist(Id, 2);}
		}
		catch(Exception e){
			System.out.println("EXCEPTION:");
			System.out.println(e.getMessage());
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public int newCustomer(int id) throws RemoteException {

		Trace.info("INFO: RM::newCustomer(" + id + ") called" );
		// Generate a globally unique ID for the new customer
		int cid = Integer.parseInt( 
				String.valueOf(Calendar.getInstance().get(Calendar.MILLISECOND)) +
				String.valueOf( Math.round( Math.random() * 100 + 1 )));
		ArrayList custID = new ArrayList<Integer>();
		custID.add(new Integer(cid));
		Trace.info("RM::newCustomer(" + cid + ") returns ID=" + cid );
		System.out.println("new customer id: "+cid);
		try{
			lockManager.Lock(id, getString(new Integer(cid)), LockManager.WRITE);
			for(int i=0;i<resourceManagerTotalNumber;i++){				 
				transactionManager.enlist(id, i);
				resourceManager[i].newCustomer(id,cid);
			}
		}
		catch(DeadlockException e){
			System.out.println("Deadlock Happened " + id);
			this.abort(id);
			throw new TransactionAbortedException("This Transaction Aborted "+id);

		}
		catch(RemoteException e){
			System.out.println("TransactionException Happened " + id);
			e.getMessage();
			this.abort(id);
			throw e;

		}
		catch(Exception e){
			System.out.println("EXCEPTION:");
			System.out.println(e.getMessage());
			e.printStackTrace();

		} 	
		//	}
		return cid; 
	}

	@Override
	public boolean newCustomer(int id, int cid) throws RemoteException {
		try{
			boolean customer=true;
			lockManager.Lock(id, getString(new Integer(cid)), LockManager.WRITE);
			for(int i=0;i<resourceManagerTotalNumber;i++){
				transactionManager.enlist(id, i);
				customer=customer && resourceManager[i].newCustomer(id,cid);
			}

			if(customer){
				System.out.println("new customer id:"+cid);
				return true;
			}
			else{
				System.out.println("could not new customer id:"+cid);
				return false;
			}
		}
		catch(DeadlockException e){
			System.out.println("Deadlock Happened " + id);
			this.abort(id);
			throw new TransactionAbortedException("This Transaction Aborted "+id);

		}
		catch(RemoteException e){
			System.out.println("TransactionException Happened " + id);
			e.getMessage();
			this.abort(id);
			throw e;

		}
		catch(Exception e){
			System.out.println("EXCEPTION:");
			System.out.println(e.getMessage());
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public int queryCars(int id, String location) throws RemoteException {
		int numCars = 0;
		try{
			transactionManager.enlist(id, 0);
			lockManager.Lock(id,"car"+location, LockManager.READ);
			numCars=resourceManager[0].queryCars(id,location);
			System.out.println("number of Cars at this location:"+numCars);
		}
		catch(DeadlockException e){
			System.out.println("Deadlock Happened " + id);
			this.abort(id);
			throw new TransactionAbortedException("This Transaction Aborted "+id);

		}
		catch(RemoteException e){
			System.out.println("TransactionException Happened " + id);
			e.getMessage();
			this.abort(id);
			throw e;

		}
		catch(Exception e){
			System.out.println("EXCEPTION:");
			System.out.println(e.getMessage());
			e.printStackTrace();
			return 0;
		}
		return numCars;
	}

	@Override
	public int queryCarsPrice(int id, String location) throws RemoteException {
		int price = 0;
		try{
			String cl="car"+location;
			transactionManager.enlist(id, 0);
			lockManager.Lock(id, cl, LockManager.READ);
			price=resourceManager[0].queryCarsPrice(id,location);
			System.out.println("Price of a car at this location:"+price);
		}
		catch(DeadlockException e){
			System.out.println("Deadlock Happened " + id);
			this.abort(id);
			throw new TransactionAbortedException("This Transaction Aborted "+id);

		}
		catch(RemoteException e){
			System.out.println("TransactionException Happened " + id);
			e.getMessage();
			this.abort(id);
			throw e;

		}
		catch(Exception e){
			System.out.println("EXCEPTION:");
			System.out.println(e.getMessage());
			e.printStackTrace();
			return 0;
		}
		return price;
	}

	@Override
	public String queryCustomerInfo(int id, int cid) throws RemoteException {  
		String bill = "";
		try{
			lockManager.Lock(id,getString(new Integer(cid)),LockManager.READ);
			transactionManager.enlist(id, 0);
			transactionManager.enlist(id, 1);
			transactionManager.enlist(id, 2);

			bill=resourceManager[0].queryCustomerInfo(id,cid);
			bill+=resourceManager[1].queryCustomerInfo(id,cid);
			bill+=resourceManager[2].queryCustomerInfo(id,cid);
			System.out.println("Customer info:"+bill);
		}
		catch(DeadlockException e){
			System.out.println("Deadlock Happened " + id);
			this.abort(id);
			throw new TransactionAbortedException("This Transaction Aborted "+id);

		}
		catch(RemoteException e){
			System.out.println("TransactionException Happened " + id);
			e.getMessage();
			this.abort(id);
			throw e;

		}
		catch(Exception e){
			System.out.println("EXCEPTION:");
			System.out.println(e.getMessage());
			e.printStackTrace();
			return null;
		}
		return bill;
	}

	@Override
	public int queryFlight(int id, int flightNumber) throws RemoteException {
		int seats = 0;
		try{
			String fl="flight"+flightNumber;
			lockManager.Lock(id, fl, LockManager.READ);
			transactionManager.enlist(id, 1);
			seats=resourceManager[1].queryFlight(id,flightNumber);
			System.out.println("Number of seats available:"+seats);
		}
		catch(DeadlockException e){
			System.out.println("Deadlock Happened " + id);
			this.abort(id);
			throw new TransactionAbortedException("This Transaction Aborted "+id);

		}
		catch(RemoteException e){
			System.out.println("TransactionException Happened " + id);
			e.getMessage();
			this.abort(id);
			throw e;

		}
		catch(Exception e){
			System.out.println("EXCEPTION:");
			System.out.println(e.getMessage());
			e.printStackTrace();
			return 0;
		}
		return seats;
	}

	@Override
	public int queryFlightPrice(int id, int flightNumber) throws RemoteException {
		int price = 0;
		try{
			lockManager.Lock(id, "flight"+flightNumber, LockManager.READ);
			transactionManager.enlist(id, 1);
			price=resourceManager[1].queryFlightPrice(id,flightNumber);
			System.out.println("Price of a seat:"+price);
		}
		catch(DeadlockException e){
			System.out.println("Deadlock Happened " + id);
			this.abort(id);
			throw new TransactionAbortedException("This Transaction Aborted "+id);

		}
		catch(RemoteException e){
			System.out.println("TransactionException Happened " + id);
			e.getMessage();
			this.abort(id);
			throw e;

		}
		catch(Exception e){
			System.out.println("EXCEPTION:");
			System.out.println(e.getMessage());
			e.printStackTrace();
			return 0;
		}
		return price;
	}

	@Override
	public int queryRooms(int id, String location) throws RemoteException {
		int numRooms = 0;
		try{
			lockManager.Lock(id,"room"+location,LockManager.READ);
			transactionManager.enlist(id, 2);
			numRooms=resourceManager[2].queryRooms(id,location);
			System.out.println("number of Rooms at this location:"+numRooms);
		}
		catch(DeadlockException e){
			System.out.println("Deadlock Happened " + id);
			this.abort(id);
			throw new TransactionAbortedException("This Transaction Aborted "+id);

		}
		catch(RemoteException e){
			System.out.println("TransactionException Happened " + id);
			e.getMessage();
			this.abort(id);
			throw e;
		}
		catch(Exception e){
			System.out.println("EXCEPTION:");
			System.out.println(e.getMessage());
			e.printStackTrace();
			return 0;
		}
		return numRooms;
	}

	@Override
	public int queryRoomsPrice(int id, String location) throws RemoteException {
		int price = 0;
		try{
			lockManager.Lock(id,"room"+location,LockManager.READ);
			transactionManager.enlist(id, 2);
			price=resourceManager[2].queryRoomsPrice(id,location);
			System.out.println("Price of Rooms at this location:"+price);
		}
		catch(DeadlockException e){
			System.out.println("Deadlock Happened " + id);
			this.abort(id);
			throw new TransactionAbortedException("This Transaction Aborted "+id);

		}
		catch(RemoteException e){
			System.out.println("TransactionException Happened " + id);
			e.getMessage();
			this.abort(id);
			throw e;

		}
		catch(Exception e){
			System.out.println("EXCEPTION:");
			System.out.println(e.getMessage());
			e.printStackTrace();
			return 0;
		}
		return price;
	}

	@Override
	public  boolean reserveCar(int id, int cid, String location)
			throws RemoteException {
		try{
			synchronized(lockobject){
				lockManager.Lock(id,"car"+location,LockManager.WRITE);
				lockManager.Lock(id, String.valueOf(cid), LockManager.WRITE);
			}
			transactionManager.enlist(id, 0);
			if(resourceManager[0].reserveCar(id,cid,location))
				System.out.println("Car Reserved");    
			else
			{
				System.out.println("Car could not be reserved.");
				return false;
			}
		}
		catch(DeadlockException e){
			System.out.println("Deadlock Happened " + id);
			this.abort(id);
			throw new TransactionAbortedException("This Transaction Aborted "+id);

		}
		catch(RemoteException e){
			System.out.println("TransactionException Happened " + id);
			e.getMessage();
			this.abort(id);
			throw e;

		}
		catch(Exception e){
			System.out.println("EXCEPTION:");
			System.out.println(e.getMessage());
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public  boolean reserveFlight(int id, int cid, int flightNumber)
			throws RemoteException {
		try{
			synchronized(lockobject){
				lockManager.Lock(id,"flight"+flightNumber,LockManager.WRITE);
				lockManager.Lock(id, String.valueOf(cid), LockManager.WRITE);
			}
			transactionManager.enlist(id, 1);
			if(resourceManager[1].reserveFlight(id,cid,flightNumber))
				System.out.println("Flight Reserved");
			else
			{
				System.out.println("Flight could not be reserved.");
				return false;
			}
		}
		catch(DeadlockException e){
			System.out.println("Deadlock Happened " + id);
			this.abort(id);
			throw new TransactionAbortedException("This Transaction Aborted "+id);

		}
		catch(RemoteException e){
			System.out.println("TransactionException Happened " + id);
			e.getMessage();
			this.abort(id);
			throw e;

		}
		catch(Exception e){
			System.out.println("EXCEPTION:");
			System.out.println(e.getMessage());
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public  boolean reserveRoom(int id, int cid, String location)
			throws RemoteException {
		try{
			synchronized(lockobject){
				lockManager.Lock(id,"room"+location,LockManager.WRITE);
				lockManager.Lock(id, String.valueOf(cid), LockManager.WRITE);
			}
			transactionManager.enlist(id, 2);
			if(resourceManager[2].reserveRoom(id,cid,location))
				System.out.println("Room Reserved");
			else
			{
				System.out.println("Room could not be reserved.");
				return false;
			}
		}
		catch(DeadlockException e){
			System.out.println("Deadlock Happened " + id);
			this.abort(id);
			throw new TransactionAbortedException("This Transaction Aborted "+id);

		}
		catch(RemoteException e){
			System.out.println("TransactionException Happened " + id);
			e.getMessage();
			this.abort(id);
			throw e;

		}
		catch(Exception e){
			System.out.println("EXCEPTION:");
			System.out.println(e.getMessage());
			e.printStackTrace();
			return false;
		}

		return true;
	}

	@Override
	public void abort(int id) throws RemoteException,
	InvalidTransactionException, InvalidTransactionException {
		transactionManager.abort(id);
		lockManager.UnlockAll(id);
		transactionStatus.put(id, "ABORT");
		Trace.info("Transaction "+id+" aborted");
	}

	@Override
	public boolean shutdown() throws RemoteException {
		Trace.info("Shutting down the system ..." );
		boolean t = transactionManager.shutdown();
		if(!t)
			return t;
		try {
			Naming.unbind("Group1" + MiddlewareServer + "Middleware");
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.exit(-1);
		return false;
	}

	@Override
	public int start() throws RemoteException {
		Trace.info("Starting Transaction ");
		int transactionID = transactionManager.start();
		transactionStatus.put(transactionID, "START");
		return transactionID;
	}

	@Override
	public int start(int id) throws RemoteException{
		Trace.info("Starting Transaction " + id);
		int transactionID = transactionManager.start();
		transactionStatus.put(transactionID, "START");
		return transactionID;
	}

	@Override
	public boolean commit(int id) throws RemoteException,
	InvalidTransactionException, TransactionAbortedException,
	InvalidTransactionException {
		lockManager.UnlockAll(id);
		return transactionManager.commit(id);
	}

	public boolean crash(String which, String where) throws RemoteException{
		if(which.equals("car"))
			return resourceManager[0].selfDestruct(where);
		else if(which.equals("flight"))
			return resourceManager[1].selfDestruct(where);
		else if(which.equals("hotel"))
			return resourceManager[2].selfDestruct(where);
		else if(which.equals("middleware"))
			return this.selfDestruct(where);
		else 
			return false;
	}

	public void connect(String which, ResourceManager rm) throws RemoteException{
		try{
			for(int resourceManagerCounter = 0; resourceManagerCounter < resourceManagerTotalNumber; resourceManagerCounter++){
				if(which.toLowerCase().equals(resourceManagers[resourceManagerCounter].toLowerCase())){
					System.out.println(which+" connected");
					resourceManager[resourceManagerCounter] = rm;
				}
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public boolean prepare(int transactionId) throws RemoteException,TransactionAbortedException,InvalidTransactionException{
		System.out.println("Vote request from Resource Manager");
		if (transactionStatus.get(transactionId).equals("COMMIT")){
			System.out.println("Vote YES");
			return true;
		}
		else{
			System.out.println("Vote NO");
			return false;
		}
	}
	public boolean selfDestruct(String where) throws RemoteException{
		crashLocation = where;
		return true;
	}

	@Override
	public boolean commit(int transactionId, boolean transactionManagerCommited)
			throws RemoteException, TransactionAbortedException,
			InvalidTransactionException, TransactionAbortedException {
		return false;
	}

	@Override
	public void abort(int transactionId, boolean transactionManagerAborted)
			throws RemoteException, InvalidTransactionException {
		
	}    
}
