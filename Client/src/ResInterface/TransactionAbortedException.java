package ResInterface;

import java.rmi.RemoteException;

public class TransactionAbortedException extends RemoteException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TransactionAbortedException() {
		// TODO Auto-generated constructor stub
	}

	public TransactionAbortedException(String s) {
		super(s);
		// TODO Auto-generated constructor stub
	}

	public TransactionAbortedException(String s, Throwable cause) {
		super(s, cause);
		// TODO Auto-generated constructor stub
	}

}
